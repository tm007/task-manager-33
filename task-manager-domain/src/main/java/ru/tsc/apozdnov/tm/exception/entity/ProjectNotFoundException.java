package ru.tsc.apozdnov.tm.exception.entity;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}