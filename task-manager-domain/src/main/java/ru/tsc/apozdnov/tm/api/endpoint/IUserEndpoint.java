package ru.tsc.apozdnov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.request.*;
import ru.tsc.apozdnov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserRegistryResponse registryUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRegistryRequest request
    );

    @NotNull
    @WebMethod
    UserRemoveResponse removeUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRemoveRequest request
    );

    @NotNull
    @WebMethod
    UserUpdateResponse updateUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUpdateRequest request
    );

    @NotNull
    @WebMethod
    UserProfileResponse showProfileUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request
    );

    @NotNull
    @WebMethod
    UserChangePasswordResponse changePassword(
            @WebParam(name = "request", partName = "request")
            @NotNull UserChangePasswordRequest request
    );

    @NotNull
    @WebMethod
    UserLockResponse lockUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLockRequest request
    );

    @NotNull
    @WebMethod
    UserUnlockResponse unlockUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUnlockRequest request
    );

}