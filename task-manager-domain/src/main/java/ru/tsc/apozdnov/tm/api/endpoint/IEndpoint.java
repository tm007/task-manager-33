package ru.tsc.apozdnov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IEndpoint {

    @NotNull
    String NAMESPACE = "http://endpoint.tm.apozdnov.tsc.ru/";

}