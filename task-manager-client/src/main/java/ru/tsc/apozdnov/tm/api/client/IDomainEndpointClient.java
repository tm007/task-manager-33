package ru.tsc.apozdnov.tm.api.client;

import ru.tsc.apozdnov.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IDomainEndpoint, IEndpointClient {

}