package ru.tsc.apozdnov.tm.api.client;

import ru.tsc.apozdnov.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {

}
