package ru.tsc.apozdnov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.client.IDomainEndpointClient;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpointClient getDomainEndpointClient() {
        return serviceLocator.getDomainEndpointClient();
    }

}