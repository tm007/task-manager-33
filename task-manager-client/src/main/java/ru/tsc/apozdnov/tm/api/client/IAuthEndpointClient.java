package ru.tsc.apozdnov.tm.api.client;

import ru.tsc.apozdnov.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {

}