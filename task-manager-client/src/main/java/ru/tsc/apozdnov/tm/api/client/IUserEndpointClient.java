package ru.tsc.apozdnov.tm.api.client;

import ru.tsc.apozdnov.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {

}