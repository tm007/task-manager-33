package ru.tsc.apozdnov.tm.api.client;

import ru.tsc.apozdnov.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {

}